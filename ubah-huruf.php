<?php
function ubah_huruf($string){
    //kode di sini
    $abjad = range("a", "z");
    $output = "";
    for($i = 0; $i < strlen($string); $i++){
        $index_abjad = array_search($string[$i], $abjad);
        $output .= $abjad[($index_abjad + 1)];
    }

    return "<p>$output</p>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>